-- Table Check
select * from pg_catalog.pg_tables;

/* 

!!!!Excluding Services at the Template Level Before Cleanup!!!!

Make sure to update the Template(Windows by Zabbix agent active) > Macro > {$SERVICE.NAME.NOT_MATCHES} with the regex value for each added service:
^CbDefenseWSC|wuauserv|BITS|ShellHWDetection|WbioSrvc|gupdate|CDPSvc|edgeupdate|clr_optimization_v4.0.30319_64|clr_optimization_v4.0.30319_32|MapsBroker|RemoteRegistry|tiledatamodelsvc|sppsvc|TrustedInstaller$

*/
-- Remove Items and Triggers

--CbDefenseWSC : Cb Defense Web Console
select * from items where name like '%CbDefenseWSC%';
delete from items where name like '%CbDefenseWSC%';
select * from triggers where description like '%CbDefenseWSC%';
delete from triggers where description like '%CbDefenseWSC%';

--wuauserv : Windows Update Service
select * from items where name like '%wuauserv%';
delete from items where name like '%wuauserv%';
select * from triggers where description like '%wuauserv%';
delete from triggers where description like '%wuauserv%';

--BITS : Background Intelligent Transfer Service
select * from items where name like '%BITS%';
delete from items where name like '%BITS%';
select * from triggers where description like '%BITS%';
delete from triggers where description like '%BITS%';

--ShellHWDetection : The Shell Hardware Detection (ShellHWDetection) service monitors and provides notification for AutoPlay hardware events. AutoPlay is a feature that detects content such as pictures, music, or video files on a removable storage device
select * from items where name like '%ShellHWDetection%';
delete from items where name like '%ShellHWDetection%';
select * from triggers where description like '%ShellHWDetection%';
delete from triggers where description like '%ShellHWDetection%';

--WbioSrvc : Windows Biometric Service
select * from items where name like '%WbioSrvc%';
delete from items where name like '%WbioSrvc%';
select * from triggers where description like '%WbioSrvc%';
delete from triggers where description like '%WbioSrvc%';

--gupdate : Google Updater
select * from items where name like '%gupdate%';
delete from items where name like '%gupdate%';
select * from triggers where description like '%gupdate%';
delete from triggers where description like '%gupdate%';

--CDPSvc : Microsoft Connected Devices Platform Service
select * from items where name like '%CDPSvc%';
delete from items where name like '%CDPSvc%';
select * from triggers where description like '%CDPSvc%';
delete from triggers where description like '%CDPSvc%';

--edgeupdate : Microsoft Edge Updater
select * from items where name like '%edgeupdate%';
delete from items where name like '%edgeupdate%';
select * from triggers where description like '%edgeupdate%';
delete from triggers where description like '%edgeupdate%';

--clr_optimization_v4.0.30319_64 : .Net Optimization Service
select * from items where name like '%clr_optimization_v4.0.30319_64%';
delete from items where name like '%clr_optimization_v4.0.30319_64%';
select * from triggers where description like '%clr_optimization_v4.0.30319_64%';
delete from triggers where description like '%clr_optimization_v4.0.30319_64%';

--clr_optimization_v4.0.30319_32 : .Net Optimization Service
select * from items where name like '%clr_optimization_v4.0.30319_32%';
delete from items where name like '%clr_optimization_v4.0.30319_32%';
select * from triggers where description like '%clr_optimization_v4.0.30319_32%';
delete from triggers where description like '%clr_optimization_v4.0.30319_32%';

--MapsBroker : Windows service for application access to downloaded maps.  This service is started on-demand by application accessing downloaded maps.  Disabling this service will prevent apps from accessing maps.
select * from items where name like '%MapsBroker%';
delete from items where name like '%MapsBroker%';
select * from triggers where description like '%MapsBroker%';
delete from triggers where description like '%MapsBroker%';

--RemoteRegistry : Enables remote users to modify registry settings on your computer. If this service is stopped, the registry can be modified only by users on your computer.
select * from items where name like '%RemoteRegistry%';
delete from items where name like '%RemoteRegistry%';
select * from triggers where description like '%RemoteRegistry%';
delete from triggers where description like '%RemoteRegistry%';

--tiledatamodelsvc : The tiledatamodelsvc service is involved in updating the content that is displayed on tiles in Start Menu.
select * from items where name like '%tiledatamodelsvc%';
delete from items where name like '%tiledatamodelsvc%';
select * from triggers where description like '%tiledatamodelsvc%';
delete from triggers where description like '%tiledatamodelsvc%';

--sppsvc : Enables the download, installation and enforcement of digital licenses for Windows and Windows applications. If the service is disabled, the operating system and licensed applications may run in a notification mode.
select * from items where name like '%sppsvc%';
delete from items where name like '%sppsvc%';
select * from triggers where description like '%sppsvc%';
delete from triggers where description like '%sppsvc%';

--TrustedInstaller : Enable installation, removal, and modification of Windows Updates and optional system components.
select * from items where name like '%TrustedInstaller%';
delete from items where name like '%TrustedInstaller%';
select * from triggers where description like '%TrustedInstaller%';
delete from triggers where description like '%TrustedInstaller%';
