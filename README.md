Zabbix Agent2

For installing on Windows Server.
For Linux Servers, GitLab CI/CD and Ansible will be used instead.

> Pending:
>     Force a metadata parameter with the script, if the value is not provided, script will not run. Will be added on the next commit.  



CDN:
    Please use zabbix_agent2_cdn.ps1 for collecting agent from the official cdn.
    
    
SCCM:
    Please use the zabbix_agent2_sccm.ps1 for collecting agent from network share on sccm.
    Path to SCCM Share: 
    `\\lic-sccmpsite2\source files$\Applications\ZabbixAgent\`
    
    HOW TO RUN:
        Open an elevated Powershell shell and set to the SCCM Share Path:
        Set-Location \\lic-sccmpsite2\source files$\Applications\ZabbixAgent\
        
        Run the Script:
        .\zabbix_agent2_automation.ps1
    
    
Please make sure to change $HostMetadataItem variable if you want to choose a different autoregistration action as configured on the Zabbix Server via Actions > Autoregistration Actions.
Please make sure to have the same file name for the zabbix agent and that the /bin and /conf path are at the parent folder of C:/zabbix when expanding.

Available Metadata at the time of this writing are:

`dev-windows-server`
`prod-windows-server`
`prod-linux-server`
`prod-macos-server`
`dev-fipay-machines`
`dev-xcenter-server`
`prod-xcenter-server`


This script will take the factory zabbix .conf file and create a custom company .conf file for autoregistration.
Then it will install the Zabbix service, set to automatic and start it.

Official Documentation:
https://www.zabbix.com/documentation/current/manual/discovery/auto_registration


Managing:
    For stopping the service run:
        `C:\zabbix\bin\zabbix_agent2.exe -x`
    For uninstalling the service run:
        `C:\zabbix\bin\zabbix_agent2.exe -d`
