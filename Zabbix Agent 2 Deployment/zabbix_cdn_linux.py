""" 
Title: Zabbix Agent OS Version 3 Agent Version 5.4.10
Author: Michael J. Acosta
Date: February 6, 2022
Description/Requirements:
    Installs Zabbix Agent for the following supported distros:
    Ubuntu 20.04 LTS (Focal), Ubuntu 18.04 LTS (Bionic), Ubuntu 16.04 LTS, and Ubuntu 14.04 LTS
    RHEL / CentOS 8, RHEL / CentOS 7, and RHEL / CentOS 6
    Debian 11 (Bullseye), Debian 10 (Buster), Debian 9 (Stretch), and Debian 8 (Jessie)
    Rasbian 10 (Rasberry Pi) and Rasbian 9 (Rasberry Pi)
"""

from urllib import request
import tarfile
import os
import shutil
import socket
import subprocess

# Define URI and HostMetaData for auto enrollment
remote_url: str = "https://cdn.zabbix.com/zabbix/binaries/stable/5.4/5.4.10/zabbix_agent-5.4.10-linux-3.0-amd64-static.tar.gz"
hostmetadata: str = "prod-linux-server"

# or if using debian/ubuntu repo:

zabbix_repo = "https://repo.zabbix.com/zabbix/5.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.4-1+ubuntu20.04_all.deb"


#################################
#    Download Zabbix Agent      #
#################################

def zabbixdownload():
    # Temp location
    local_file: str = "/tmp/zabbix_agent.tar.gz"
    # Download remote and save locally
    request.urlretrieve(remote_url, local_file)


def zabbixrepo():
    zabbix_deb = "/tmp/zabbix-release_5.4-1+ubuntu20.04_all.deb"
    request.urlretrieve(zabbix_repo, zabbix_deb)


#################################
#   Decompress Zabbix Agent     #
#################################
def zabbixdecompress():
    # Open compressed TAR.GZ
    targzfile = tarfile.open("/tmp/zabbix_agent.tar.gz")
    # Extract TAR.GZ file
    targzfile.extractall("/tmp/zabbix_agent")
    targzfile.close()


#################################
#    Configure Zabbix Agent     #
#################################
def zabbixconf():
    # Zabbix Server
    zabbix_server: str = "Server=127.0.0.1"
    zabbix_server_replace: str = "Server=cmk-zabbix1"
    # Opening our text file in read only mode using the open() function
    with open(r"/etc/zabbix/zabbix_agentd.conf", "r") as file:
        # Reading the content of the file
        # using the read() function and storing
        # them in a new variable
        data = file.read()
        # Searching and replacing the text
        # using the replace() function
        data = data.replace(zabbix_server, zabbix_server_replace)
    # Opening our text file in write only
    # mode to write the replaced content
    with open(r"/etc/zabbix/zabbix_agentd.conf", "w") as file:
        # Writing the replaced data in our
        # text file
        file.write(data)

    # Zabbix ServerActive
    zabbix_serveractive: str = "ServerActive=127.0.0.1"
    zabbix_serveractive_replace: str = "ServerActive=cmk-zabbix1"
    # Opening our text file in read only mode using the open() function
    with open(r"/etc/zabbix/zabbix_agentd.conf", "r") as file:
        # Reading the content of the file
        # using the read() function and storing
        # them in a new variable
        data = file.read()
        # Searching and replacing the text
        # using the replace() function
        data = data.replace(zabbix_serveractive, zabbix_serveractive_replace)
    # Opening our text file in write only
    # mode to write the replaced content
    with open(r"/etc/zabbix/zabbix_agentd.conf", "w") as file:
        # Writing the replaced data in our
        # text file
        file.write(data)

    # Node Hostname
    node: str = socket.gethostname()
    zabbix_node: str = "Hostname=Zabbix server"
    zabbix_node_replace: str = f"Hostname={node}"
    # Opening our text file in read only mode using the open() function
    with open(r"/etc/zabbix/zabbix_agentd.conf", "r") as file:
        # Reading the content of the file
        # using the read() function and storing
        # them in a new variable
        data = file.read()
        # Searching and replacing the text
        # using the replace() function
        data = data.replace(zabbix_node, zabbix_node_replace)
    # Opening our text file in write only
    # mode to write the replaced content
    with open(r"/etc/zabbix/zabbix_agentd.conf", "w") as file:
        # Writing the replaced data in our
        # text file
        file.write(data)

    # HostMetadata
    zabbix_meta: str = "# HostMetadata="
    zabbix_meta_replace: str = f"HostMetadata={hostmetadata}"
    # Opening our text file in read only mode using the open() function
    with open(r"/etc/zabbix/zabbix_agentd.conf", "r") as file:
        # Reading the content of the file
        # using the read() function and storing
        # them in a new variable
        data = file.read()
        # Searching and replacing the text
        # using the replace() function
        data = data.replace(zabbix_meta, zabbix_meta_replace)
    # Opening our text file in write only
    # mode to write the replaced content
    with open(r"/etc/zabbix/zabbix_agentd.conf", "w") as file:
        # Writing the replaced data in our
        # text file
        file.write(data)

        # Enable Remote Commands
        # Append-adds at last
        file1 = open("/etc/zabbix/zabbix_agentd.conf", "a")  # append mode
        file1.write(
            "\n# Enable remote commands. \n AllowKey=system.run[*] \n Timeout=30 \n UnsafeUserParameters=1\n")
        file1.close()


#################################
#      Move Zabbix Agent        #
#################################
def zabbixmove():
    # Move configured Zabbix Agent
    src: str = "/tmp/zabbix_agent"
    dst: str = "/opt/zabbix/zabbix_agent"

    # Check if file already exists
    if os.path.isdir(dst):
        print(src, 'exists in the destination path!')
        shutil.rmtree(dst)

    elif os.path.isfile(dst):
        os.remove(dst)
        print(src, 'deleted in', dst)

    # Move the content
    # source to destination
    shutil.move(src, dst)

    print(src, 'has been moved!')

    # Print new path of file
    print("Destination path:", dst)


#################################
#      Install Zabbix Agent     #
#################################

def zabbixinstall():
    subprocess.call(["sudo", "dpkg", "-i", "/tmp/zabbix-release_5.4-1+ubuntu20.04_all.deb"])
    subprocess.call(["sudo", "apt", "update", "-y"])
    subprocess.call(["sudo", "apt", "install", "-y", "zabbix-agent"])
    subprocess.call(["sudo", "systemctl", "restart", "zabbix-agent"])


#################################
#            EXECUTE            #
#################################
print("Starting the Zabbix Agent 2 installation...")
# zabbixdownload() //Installing from pkg
zabbixrepo()
zabbixinstall()
# zabbixdecompress() //Installing from pkg
zabbixconf()
# zabbixmove() //Installing from pkg
