<# 

Title: Zabbix Agent 2 Install from Network Share
Author: Michael Acosta
Date: 6/4/2021

This script consists of 4 functions:
    
    1. Install Zabbix from archive
    2. Configure Zabbix
    3. Start Zabbix
    4. Set Zabbix Recovery


Edit: 7/2/2021
    Added support for Windows 2008 without WMF 5.1, .Net 4.5 and running Powershell 2.0.
    Expand-Archive and [System.IO.Compression.ZipFile]::ExtractToDirectory() are replaced by a vbs script that can extract the files for Servers without WMF 5.1.
    Added conditionals and to silently continue for path checks if a previous install of Zabbix is present.
Edit: 10/30/2021
    Included a sc.exe Functions called 'zabbixrecovery' since none of the powershell cmdlet provide an option for setting the recovery for a service.
    This will setup 1st,2nd and 3rd attempt to do a 'Restart Service' if it fails to keep running for whatever reason. Recheck if recovery has been set proper by running:
    sc \\$Server qfailure $Service
Edit: 1/30/2021
    Updated Zabbix Agent to version 5.4.9

#>


#---- VARIABLES ----

#Make sure you update both the file and url variables with the proper zip file name if upgrading/downgrading!!!
$file = "zabbix_agent2-5.4.9-windows-amd64-openssl-static.zip"
$zaurl = "https://cdn.zabbix.com/zabbix/binaries/stable/5.4/5.4.9/zabbix_agent2-5.4.9-windows-amd64-openssl-static.zip"

#System Variables
$zabbixpath = "C:\zabbix\"
#conf variables
$zabbixconf_factory="C:\zabbix\conf\zabbix_agent2.conf"
$zabbixconf_company="C:\zabbix\bin\zabbix_agent2.win.conf"
$Server="Server=cmk-zabbix1"
$ServerActive="ServerActive=cmk-zabbix1"
$ComputerName ="$env:COMPUTERNAME"
$Global:Hostname="Hostname=$ComputerName"
#CHANGE THE FOLLOWING TO THE RIGHT METADATA
$HostMetadataItem="HostMetadata=prod-windows-server"


# ---- FUNCTIONS -----


function deleteoldinstall {
    $OldZabbix = Test-Path -Path 'C:\zabbix\' -ErrorAction SilentlyContinue
    
    if ($OldZabbix) {
        Stop-Service 'Zabbix Agent 2' -ErrorAction SilentlyContinue
        Start-Process "cmd.exe" "/c C:\zabbix\bin\zabbix_agent2.exe --uninstall" -Wait
        Remove-Item -Path C:\zabbix* -Recurse -Force -ErrorAction SilentlyContinue
    }else {
        Write-Host "No previous version of Zabbix found...."
        }
    }
    

function zabbixInstall {

    
    $tmp = "$env:TEMP\$file"
    $cli = New-Object System.Net.WebClient;
    $cli.Headers['User-Agent'] = 'myUserAgentString';
    $cli.DownloadFile($zaurl, $tmp)
    New-Item -ItemType "directory" -Path $zabbixpath -Force -ErrorAction SilentlyContinue
    $extractorscript = "$env:TEMP\" + "extractor.bat"




#For new builds of Windows Powershell
$ArchiveModule = Test-Path C:\Windows\system32\WindowsPowerShell\v1.0\Modules\Microsoft.PowerShell.Archive\Microsoft.PowerShell.Archive.psd1 -ErrorAction SilentlyContinue

if ($ArchiveModule) {
    Write-Host "Microsoft.PowerShell.Archive is available and will be used for extracting zip..."
    Import-Module Microsoft.PowerShell.Archive
    Expand-Archive -Path $tmp -DestinationPath $zabbixpath -Force
}else{

    Write-Host "Microsoft.PowerShell.Archive not found for using Expand-Archive cmdlt, using VBS Script..."
    #For older builds of Windows Powershell
@"
@echo off
setlocal
cd /d %~dp0
Call :UnZipFile "$zabbixpath" "$tmp" 
exit /b

:UnZipFile <ExtractTo> <newzipfile>
set vbs="%temp%\_.vbs"
if exist %vbs% del /f /q %vbs%
>%vbs%  echo Set fso = CreateObject("Scripting.FileSystemObject")
>>%vbs% echo If NOT fso.FolderExists(%1) Then
>>%vbs% echo fso.CreateFolder(%1)
>>%vbs% echo End If
>>%vbs% echo set objShell = CreateObject("Shell.Application")
>>%vbs% echo set FilesInZip=objShell.NameSpace(%2).items
>>%vbs% echo objShell.NameSpace(%1).CopyHere(FilesInZip)
>>%vbs% echo Set fso = Nothing
>>%vbs% echo Set objShell = Nothing
cscript //nologo %vbs%
if exist %vbs% del /f /q %vbs%
"@ | Out-File -FilePath $extractorscript -Encoding utf8 -Force -Confirm:$false
Start-Process "cmd.exe" "/c $extractorscript" -Wait
Remove-Item -Path $extractorscript
}

$ExecuteExtractorGood = Test-Path 'C:\zabbix\bin\zabbix_agent2.exe' -ErrorAction SilentlyContinue

if ($ExecuteExtractorGood) {
    Write-Host "VBS Extractor Worked, continuing with installation"
}else {
    Write-Host "VBS Script failed, attempting with custom function..."
    function Expand-ZIPFile {
        param (
            [string]$file,
            [string]$destination
        )
    
        if (!$destination) {
            $destination = [string](Resolve-Path $file)
            $destination = $destination.Substring(0, $destination.LastIndexOf('.'))
            mkdir $destination | Out-Null
        }
        $shell = New-Object -ComObject Shell.Application
        #$shell.NameSpace($destination).CopyHere($shell.NameSpace($file).Items(), 16);
        $zip = $shell.NameSpace($file)
        foreach ($item in $zip.items()) {
            $shell.Namespace($destination).CopyHere($item)
        }
    }

    Expand-ZIPFile $tmp $zabbixpath

    $Zabbixexists = Test-Path 'C:\zabbix\bin\zabbix_agent2.exe' -ErrorAction SilentlyContinue

    if ($Zabbixexists) {
        Write-Host "Zabbix Extracted Succesfully..."
    }else {
        Write-Host "Unable to extract Zabbix Agent 2. Aborting."
        exit 1
    }

}


     Remove-Item -Path $tmp
    
}


function zabbixconf {
#Needs additional work for a better CLI Output.
    (Get-Content $zabbixconf_factory) | Foreach-Object {
    $_ -replace 'Server=127.0.0.1', $Server `
       -replace 'ServerActive=127.0.0.1', $ServerActive `
       -replace 'Hostname=Windows host', $Hostname `
       -replace '# HostMetadata=', $HostMetadataItem `
       -replace '# LogType=file', "LogType=file" `
    } | Set-Content $zabbixconf_company
    Remove-Item $zabbixconf_factory
    Add-Content $zabbixconf_company ""
    Add-Content $zabbixconf_company "# The following 3 lines will enable remote commands."
    Add-Content $zabbixconf_company "AllowKey=system.run[*]"
    Add-Content $zabbixconf_company "Timeout=30"
    Add-Content $zabbixconf_company "UnsafeUserParameters=1"
    Add-Content $zabbixconf_company "LogFile=c:\zabbix\zabbix_agent2.log"

}


function zabbixstart {

    #Needs more work for a better CLI output
   C:\zabbix\bin\zabbix_agent2.exe -i -c C:\zabbix\bin\zabbix_agent2.win.conf
   C:\zabbix\bin\zabbix_agent2.exe -s


}

function zabbixrecovery{
    [alias('Set-Recovery')]
    param
    (
        [string] [Parameter(Mandatory=$true)] $ServiceDisplayName,
        [string] [Parameter(Mandatory=$false)] $Server,
        [string] $action1 = "restart",
        [int] $time1 =  30000, # in miliseconds
        [string] $action2 = "restart",
        [int] $time2 =  30000, # in miliseconds
        [string] $actionLast = "restart",
        [int] $timeLast = 30000, # in miliseconds
        [int] $resetCounter = 4000 # in seconds
    )
    
    $services = Get-CimInstance -ClassName 'Win32_Service'| Where-Object {$_.DisplayName -imatch $ServiceDisplayName}
    $action = $action1+"/"+$time1+"/"+$action2+"/"+$time2+"/"+$actionLast+"/"+$timeLast
    foreach ($service in $services){
        # https://technet.microsoft.com/en-us/library/cc742019.aspx
        sc.exe $server failure $($service.Name) actions= $action reset= $resetCounter
    }
} 


###############
#   PortQry   #
###############

$Path="C:\PortQry\"
$Exec="PortQry.exe"
$PortQry = $Path + $Exec
function portCheck{
    $CurrentPortQry = Test-Path -Path $PortQry -ErrorAction SilentlyContinue
    if ($CurrentPortQry) {
        Write-Host "PortQry Found, checking ports..."    
            }
        else {
            Write-Host "No previous version of PortQry found. Installing..."
            Remove-Item -Path $Path -Recurse -Force -ErrorAction SilentlyContinue
            New-Item -Path $Path -ItemType Directory
            Invoke-WebRequest -Uri https://steve-madden-ltd.s3.amazonaws.com/Windows/PortQry/PortQry.exe -OutFile $PortQry
      }
}    
function portEnum{
    #Check ports. Add as many ports needed.

    & $PortQry @('-n','cmk-zabbix1','-e','10050')
    & $PortQry @('-n','cmk-zabbix1','-e','10051')

}

# ---- EXECUTE ----
Write-Output "Starting the Zabbix Agent 2 installation..."
deleteoldinstall
zabbixInstall
zabbixconf
zabbixstart
zabbixrecovery -ServiceDisplayName 'Zabbix Agent 2'

#Verify that failures have been set.
sc.exe qfailure 'Zabbix Agent 2'

#Check Ports.
#portCheck
#portEnum


exit 0

