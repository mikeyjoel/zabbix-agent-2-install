<# 

Title: Zabbix Agent Install and configure Zabbix DHCP.
Author: Michael Acosta
Date: 6/4/2021

This script consists of 4 functions:
    
    1. Install Zabbix from archive.
    2. Configure Zabbix.
    3. Create DHCP script.
    3. Start Zabbix.
    4. Set Zabbix Recovery.

Note* Timeout needs to be set in your Zabbix Server to 30 at /etc/zabbix/zabbix_server.conf and then systemctl restart zabbix-server.service
Edit: 6/4/2021
    Make sure that the $zaurl is pointing to the Zabbix Agent 1, not 2 and change the $file to the file name accordingly.
    This will not work with Zabbix Agent 2. Tested working with Zabbix 5.4.X.
#>


#---- VARIABLES ----

#Make sure you update both the file and url variables with the proper zip file name if upgrading/downgrading!!!
$file = "zabbix_agent-5.4.7-windows-amd64-openssl.zip"
$zaurl = "https://cdn.zabbix.com/zabbix/binaries/stable/5.4/5.4.7/zabbix_agent-5.4.7-windows-amd64-openssl.zip"

#Installation Path
$zabbixpath = "C:\zabbix\"
#conf,log,other variables
$zabbixconf_factory="C:\zabbix\conf\zabbix_agentd.conf"
$zabbixconf_company="C:\zabbix\bin\zabbix_agentd.win.conf"
$zabbixconf_logs="LogType=system"
$Server="Server=cmk-zabbix1"
$ServerActive="ServerActive=cmk-zabbix1"
$ComputerName ="$env:COMPUTERNAME"
$Global:Hostname="Hostname=$ComputerName"
#CHANGE THE FOLLOWING TO THE RIGHT METADATA FOR AUTOREGISTRATION AFTER THE = SYMBOL
$HostMetadataItem="HostMetadata=prod-dhcp-server"


# ---- FUNCTIONS -----


function deleteoldinstall {
    $OldZabbix = Test-Path -Path 'C:\zabbix\' -ErrorAction SilentlyContinue
    
    if ($OldZabbix) {
        Stop-Service 'Zabbix Agent' -ErrorAction SilentlyContinue
        Start-Process "cmd.exe" "/c C:\zabbix\bin\zabbix_agentd.exe --uninstall" -Wait
        Remove-Item -Path C:\zabbix\ -Recurse -Force -ErrorAction SilentlyContinue
    }else {
        Write-Host "No previous version of Zabbix found...."
        }
    }
    

function zabbixInstall {

    
    $tmp = "$env:TEMP\$file"
    $cli = New-Object System.Net.WebClient;
    $cli.Headers['User-Agent'] = 'myUserAgentString';
    $cli.DownloadFile($zaurl, $tmp)
    New-Item -ItemType "directory" -Path $zabbixpath -Force -ErrorAction SilentlyContinue
    $extractorscript = "$env:TEMP\" + "extractor.bat"




#For new builds of Windows Powershell
$ArchiveModule = Test-Path C:\Windows\system32\WindowsPowerShell\v1.0\Modules\Microsoft.PowerShell.Archive\Microsoft.PowerShell.Archive.psd1 -ErrorAction SilentlyContinue

if ($ArchiveModule) {
    Write-Host "Microsoft.PowerShell.Archive is available and will be used for extracting zip..."
    Import-Module Microsoft.PowerShell.Archive
    Expand-Archive -Path $tmp -DestinationPath $zabbixpath -Force
}else{

    Write-Host "Microsoft.PowerShell.Archive not found for using Expand-Archive cmdlt, using VBS Script..."
    #For older builds of Windows Powershell
@"
@echo off
setlocal
cd /d %~dp0
Call :UnZipFile "$zabbixpath" "$tmp" 
exit /b

:UnZipFile <ExtractTo> <newzipfile>
set vbs="%temp%\_.vbs"
if exist %vbs% del /f /q %vbs%
>%vbs%  echo Set fso = CreateObject("Scripting.FileSystemObject")
>>%vbs% echo If NOT fso.FolderExists(%1) Then
>>%vbs% echo fso.CreateFolder(%1)
>>%vbs% echo End If
>>%vbs% echo set objShell = CreateObject("Shell.Application")
>>%vbs% echo set FilesInZip=objShell.NameSpace(%2).items
>>%vbs% echo objShell.NameSpace(%1).CopyHere(FilesInZip)
>>%vbs% echo Set fso = Nothing
>>%vbs% echo Set objShell = Nothing
cscript //nologo %vbs%
if exist %vbs% del /f /q %vbs%
"@ | Out-File -FilePath $extractorscript -Encoding utf8 -Force -Confirm:$false
Start-Process "cmd.exe" "/c $extractorscript" -Wait
Remove-Item -Path $extractorscript
}

$ExecuteExtractorGood = Test-Path 'C:\zabbix\bin\zabbix_agentd.exe' -ErrorAction SilentlyContinue

if ($ExecuteExtractorGood) {
    Write-Host "VBS Extractor Worked, continuing with installation"
}else {
    Write-Host "VBS Script failed, attempting with custom function..."
    function Expand-ZIPFile {
        param (
            [string]$file,
            [string]$destination
        )
    
        if (!$destination) {
            $destination = [string](Resolve-Path $file)
            $destination = $destination.Substring(0, $destination.LastIndexOf('.'))
            mkdir $destination | Out-Null
        }
        $shell = New-Object -ComObject Shell.Application
        #$shell.NameSpace($destination).CopyHere($shell.NameSpace($file).Items(), 16);
        $zip = $shell.NameSpace($file)
        foreach ($item in $zip.items()) {
            $shell.Namespace($destination).CopyHere($item)
        }
    }

    Expand-ZIPFile $tmp $zabbixpath

    $Zabbixexists = Test-Path 'C:\zabbix\bin\zabbix_agentd.exe' -ErrorAction SilentlyContinue

    if ($Zabbixexists) {
        Write-Host "Zabbix Extracted Succesfully..."
    }else {
        Write-Host "Unable to extract Zabbix Agent. Aborting."
        exit 1
    }

}


     Remove-Item -Path $tmp
    
}


function zabbixconf {
#Needs additional work for a better CLI Output.
    (Get-Content $zabbixconf_factory) | Foreach-Object {
    $_ -replace '# LogType=file', $zabbixconf_logs`
       -replace 'Server=127.0.0.1', $Server `
       -replace 'ServerActive=127.0.0.1', $ServerActive `
       -replace 'Hostname=Windows host', $Hostname `
       -replace '# HostMetadata=', $HostMetadataItem `
       
    } | Set-Content $zabbixconf_company
    Remove-Item $zabbixconf_factory
    Add-Content $zabbixconf_company ""
    Add-Content $zabbixconf_company "# The following 4 lines will enable monitoring of the DHCP role for Windows Server."
    Add-Content $zabbixconf_company "AllowKey=system.run[*]"
    Add-Content $zabbixconf_company "Timeout=30"
    Add-Content $zabbixconf_company "UnsafeUserParameters=1"
    Add-Content $zabbixconf_company "UserParameter=dhcp[*],powershell -NoProfile -ExecutionPolicy Bypass -File `"C:\zabbix\conf\zabbix_dhcp_scope.ps1`" `"`$1`" `"`$2`""
    #Get-Content $zabbixconf_company

}

function zabbixDHCP{
    
$DHCPPS1="C:\zabbix\conf\zabbix_dhcp_scope.ps1"

@"
# Zabbix Windows DHCP Server Script for Template.
# Description: Query DHCP Server information
# 
# This script is intended for use with Zabbix > 3.x
# Make sure to make Zabbix Passive Agent and AllowKey.
#
#
# USAGE:
#   as a script:    C:\WINDOWS\system32\windowspowershell\v1.0\powershell.exe -command  "& C:\Zabbix\zabbix_dhcp_scope.ps1 <ITEM_TO_QUERY> <SCOPE>"
#   as an item:     dhcp[<ITEM_TO_QUERY>,<SCOPE>]
#
# Add to Zabbix Agent conf
#   
#       UnsafeUserParameters=1
#       UserParameter=dhcp[*],powershell -NoProfile -ExecutionPolicy Bypass -File "C:\zabbix\conf\zabbix_dhcp_scope.ps1" "`$1" "`$2"



`$ITEM = [string]`$args[0]
`$ID = [string]`$args[1]

switch (`$ITEM) {
  "Discovery" {
    # Open JSON object
    `$output =  "{``"data``":["
      `$query = Get-DhcpServerv4ScopeStatistics | Where {`$_.Free -gt 0} | Select-Object ScopeId
      `$count = `$query | Measure-Object
      `$count = `$count.count
      foreach (`$object in `$query) {
        `$Id = [string]`$object.ScopeId
        if (`$count -eq 1) {
          `$output = `$output + "{``"{#SCOPEID}``":``"`$Id``"}"
        } else {
          `$output = `$output + "{``"{#SCOPEID}``":``"`$Id``"},"
        }
        `$count--
    }
    # Close JSON object
    `$output = `$output + "]}"
    Write-Host `$output
  }
   
   "ScopeFree" {
  `$query = Get-DhcpServerv4ScopeStatistics | Where-Object {`$_.ScopeId -like "*`$ID*"} | Select-Object Free
  foreach (`$object in `$query) {
  `$Free = [string]`$object.Free}
  `$Free
    }

   "ScopeInUse" {
  `$query = Get-DhcpServerv4ScopeStatistics | Where-Object {`$_.ScopeId -like "*`$ID*"} | Select-Object InUse
  foreach (`$object in `$query) {
  `$InUse = [string]`$object.InUse}
  `$InUse
    }
    
   "ScopePercentageInUse" {
  `$query = Get-DhcpServerv4ScopeStatistics | Where-Object {`$_.ScopeId -like "*`$ID*"} | Select-Object PercentageInUse
  foreach (`$object in `$query) {
  `$PercentageInUse = [string]`$object.PercentageInUse}
  `$PercentageInUse
    } 

   "ScopeReserved" {
  `$query = Get-DhcpServerv4ScopeStatistics | Where-Object {`$_.ScopeId -like "*`$ID*"} | Select-Object Reserved
  foreach (`$object in `$query) {
  `$Reserved = [string]`$object.Reserved}
  `$Reserved
    } 
 
  default {
      Write-Host "-- ERROR -- : Need an option!"
  }
}
"@ | Out-File -FilePath $DHCPPS1 -Encoding ascii -Force -Confirm:$false
    
}


function zabbixstart {

    #Needs more work for a better CLI output
   C:\zabbix\bin\zabbix_agentd.exe -i -c C:\zabbix\bin\zabbix_agentd.win.conf
   C:\zabbix\bin\zabbix_agentd.exe -s


}

function zabbixrecovery{
    [alias('Set-Recovery')]
    param
    (
        [string] [Parameter(Mandatory=$true)] $ServiceDisplayName,
        [string] [Parameter(Mandatory=$false)] $Server,
        [string] $action1 = "restart",
        [int] $time1 =  30000, # in miliseconds
        [string] $action2 = "restart",
        [int] $time2 =  30000, # in miliseconds
        [string] $actionLast = "restart",
        [int] $timeLast = 30000, # in miliseconds
        [int] $resetCounter = 4000 # in seconds
    )
    
    $services = Get-CimInstance -ClassName 'Win32_Service'| Where-Object {$_.DisplayName -imatch $ServiceDisplayName}
    $action = $action1+"/"+$time1+"/"+$action2+"/"+$time2+"/"+$actionLast+"/"+$timeLast
    foreach ($service in $services){
        # https://technet.microsoft.com/en-us/library/cc742019.aspx
        sc.exe $server failure $($service.Name) actions= $action reset= $resetCounter
    }
} 




# ---- EXECUTE ----
Write-Output "Starting the Zabbix Agent DHCP installation..."
deleteoldinstall
zabbixInstall
zabbixconf
zabbixDHCP
zabbixstart
zabbixrecovery -ServiceDisplayName 'Zabbix Agent'


exit 0




